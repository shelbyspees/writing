# Sophie's Voices

Sophie hears voices BUT  
The voices are distracting SO  
She learned to get things done anyway BUT  
Recently she was diagnosed with depression SO  
She started taking antidepressants and they help BUT  
The meds got rid of Sophie's voices and now she's more productive SO  
People pressure her to do great things BUT  
She's never worked a real job SO  
She interviews a bunch BUT  
Nobody wants to hire her SO  
She asks her reporter friend to help her get freelance gigs BUT  
The friend gets jealous when Sophie's articles blow up SO  
The friend disappears and Sophie is lonely without friends or voices BUT  
Sophie doesn't want to choose between her budding journalism career and her sanity SO  
She continues working crazy hard BUT  
She burns out and nearly dies SO  
She decides to stop taking the meds and become a hermit BUT   
Her depression is still a problem SO  
She looks for ways to keep herself functional BUT  
Now she knows how much she's capable of accomplishing SO  
She uses her new fame to write about embracing mental illness