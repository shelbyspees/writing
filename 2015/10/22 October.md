Terra woke up sweaty with the sheets twisted around her legs. She hadn't been sleeping well lately. The humidity was driving her mad. 

4:11am. She wanted to be at work at 6. Lately she'd been getting to work at 9:30. Her good habits were long gone.

She rolled out of bed and rested her hand against the wall for balance as she stood up. Vertigo held her for a moment. 