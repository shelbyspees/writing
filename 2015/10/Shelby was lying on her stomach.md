Shelby was lying on her stomach, typing on her laptop. She was feeling the bloat from the 20oz of real Italian gelato she inhaled a few hours earlier.

"It's amazing that I'm not fatter than this," she thought.

Some time in the last six months, Shelby got it into her head that she should try writing fiction. The inciting moment was when Brian, a guy she was dating, emailed her a script he wrote as requested by the director of a web series. It made her want to barf.

Basically, you can't fit a gun there. Just, so much wrong.

Before that moment, she never felt like a creative. Sure, she played violin, but playing music is different from writing music. She could write boring essays and blog posts well enough but she never had any decent ideas for a story.
  
Reading that script made her question herself. If this is what passes as writing? She can surely do better.

She started reading writers.stackexchange.com and from there heard about Stephen King's autobiographical audiobook *On Writing*. It was the perfect listen for her 2-3 hours of commuting each day. Sure, King had always felt the bite of the writing bug, but his success is due to his relentless effort. That guy worked his ass off and never let rejection get him down.

So if writing was something she could learn, then maybe she should learn it. From there she tried describing scenes in her normal life. While she enjoyed manipulating the written word, Shelby struggled with story ideas--at least ones that didn't make her gag.

So another three months later, here she lied. "Lied" is a great word for it actually. Isn't fiction all lies? But it's also a way to highlight truth. Fiction is relentlessly human.

Shelby hated the feeling of her fingernails hitting the keys. It was simultaneously sensitive and cumbersome.

Immediately she judged her word choice. "That's counterproductive," she told herself, but the nitpicking judge inside her wouldn't let up.

She's crazy. She has to be. Why would anybody be this self-destructive in these circumstances? She has no reason to feel anything but relaxed.

Does she feel...trapped?

What is this pressure? Where is it coming from? Why is it hard for her to deal with being important?

Shelby can't grow up. She can't take care of herself. But she has to. She is. This is the kind of care she takes. She cares for herself least of all.

Drugs. The sugar is a drug for her. The numbness and energy and fullness. Instant serotonin spike. She can't get that from other parts of her life? Guess not.

Shelby knows she'd be good looking if she took care of herself. The acne and flab would go away. She'd feel more positive and energetic. She'd be able to focus better. Is this a kind of self-sabotage? Most likely, yes.

That last paragraph sounded a lot like this part in *Carrie*. She would have been pretty if things had turned out differently.

Is Shelby trying to be less pretty? Less feminine? She judges women at work who wear pencil skirts or sheath dresses with four inch heels. Why does she judge them so hard? Because she'd look like a monster in that.

When Shelby was a kid she was skinny. She didn't like most food and she spent all her time outside.

It's been so long since that part of her life. She doesn't think she can go back.

"Maybe it's the hormones," she thought. "That's pretty much when my appetite completely changed and I starting sleeping all the time." She still felt like sleeping all the time. She should have been sleeping then.

What's the point of washing my face if I'm already breaking out from the ice cream?

So. Fat.