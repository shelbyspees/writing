Jeff got up early that Sunday. He wanted to make a good breakfast for Cheryl for her first (pre-) Mother's Day. Her favorite veggie omelette, some slow cooked oatmeal. Lots of nutrients for that growing baby.

Jeff didn't know how that Mother's Day would turn out to be the worst he and Cheryl would ever have.

No, scratch that. Second worst. 

So Jeff slipped out of bed. He shut the door softly behind him. He padded to the kitchen. 

The eggs perfectly cracked, the pan perfectly hot, Jeff managed a beautifully organized setting for his beautifully organized wife. He poured her favorite green probiotic juice (it tasted like mango) and brought the whole spread to their bedroom on the Ikea tray.

He set the tray on the tray table he folded out and leaned over to push Cheryl's hair out of her face. 

"Hey baby, happy Mother's Day."

Cheryl moaned and pulled the covers up over her face.

"I made you breakfast," Jeff said in his sing-song voice.


... 

Cheryl's face wrinkled in pain. "My stomach hurts."

"Are you hungry? Or is it something else?" Jeff immediately thought of the baby. 

"Let me go to the bathroom," Cheryl said. She pushed off the covers and revealed the large blood stain underneath. 

Cheryl screamed. 

... 

Cheryl wanted the baby. She so wanted to be a mother. Her head hurt thinking about the implications of waking up to this.

They had been trying for a baby for three years, so when she finally got pregnant it was such a relief. She 

... 

Jeff cursed to himself. It didn't help. His stomach knots tightened the more he thought about Cheryl and the baby. Baby no more, he thought morbidly. Just a memory, a fruitless hope long gone. 

He remembered the tray of food he left in the bedroom. It was probably cold by now. He had tried to make this day fun for Cheryl. He had wanted his wife's first Mother's Day done right.

Now thoughts of his ruined plans made way for the pain he felt. Pain he felt for Cheryl. He wanted to be angry but what could he blame? Nature? The universe? Fate?

Part of him wanted to blame Cheryl. His logical side knew she'd feel even worse than he did. For now that side was winning, but a deep resentful part of him tried to direct his pain and anger at her. It made no sense. Maybe this part of him just needed an outlet, a target. Throwing the negativity at her lessened the burden for himself. 

But this side of him was safely tucked away behind a solid foundation of trust and love and shared experiences. 

